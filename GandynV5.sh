#/bin/bash

# #####
# Author: Corentin Bettiol
# Version: 2
# Sources: https://gitlab.com/sodimel/gandynV5/
#
# Realized with the help of http://doc.livedns.gandi.net/ :)
# #####

# #####
# Variables
# #####

APIKEY='YOURAPIKEYHERE'
DOMAIN='YOURDOMAINURLHERE'
DEBUG=False

# #####
# Script
# #####

newIp=`dig +short myip.opendns.com @resolver1.opendns.com`

zoneDNS=`curl -s -H "X-Api-Key: $APIKEY" \
	https://dns.api.gandi.net/api/v5/domains/$DOMAIN/records`

zoneDNS=$(echo $zoneDNS | jq "map(. | if (.rrset_type == \"A\") then . + { \"rrset_values\": [ \"$newIp\" ] } else . end)")

if [ "$DEBUG" = "True" ]; # echo of the new dns zone that will be sent to the gandi api
then
	echo "New Ip:" $newIp
	echo "New zoneDNS data:"
	echo $zoneDNS | jq .
fi

curl -s -X PUT -H "Content-Type: application/json" \
	-H "X-Api-Key: $APIKEY" \
	-d "{\"items\": $zoneDNS}" \
	https://dns.api.gandi.net/api/v5/domains/$DOMAIN/records \
	| jq .
